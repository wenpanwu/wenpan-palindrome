package com.atb.palindrome;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PalindromeService {

    /**
     * Adding the digits to the right in reverse order to compose a palindrome
     * @param left
     * @param even the original number is even
     * @return the palindrome derived from the left
     */
    private long getPalindrome(long left, boolean even) {
        long res = left;
        //if not even, skip the right most digit from the calculation of the right part
        if(!even) left = left/10;
        while(left>0) {
            res = res * 10 + left % 10;
            left /= 10;
        }
        return res;
    }
    /**
     * @param x int
     * @return boolean
     */
    private boolean isPalindrome(long x) {
        long t = x, rev = 0;
        while (t > 0) {
            rev = 10 * rev + t % 10;
            t /= 10;
        }
        return rev == x;
    }

    public long getClosestPalindrome1(long num) {
        for (int i = 1;; i++) {
            if (isPalindrome(num - i))
                return (num - i);
            if (isPalindrome(num + i))
                return (num + i);
        }

    }

    public long getClosestPalindrome2(long num) {
        //e.g. 12345
        String numStr = Long.toString(num); //no leading zero
        int len = numStr.length();
        int mid = len%2==0 ? len/2-1 : len/2;
        long left = Long.parseLong(numStr.substring(0, mid+1));
        List<Long> candidateList = new ArrayList<>();
        candidateList.add(getPalindrome(left, len%2==0)); //case 0, 12321
        candidateList.add(getPalindrome(left+1, len%2==0)); //case 1, 12421
        candidateList.add(getPalindrome(left-1, len%2==0)); //case 2, 12221
        candidateList.add((long)Math.pow(10, len-1) - 1 ); //case 3, 9999
        candidateList.add((long)Math.pow(10, len) + 1 ); //case 4, 100001
        long diff = Long.MAX_VALUE, res = 0;
        for(long cand: candidateList) {
            if(cand==num) continue;
            if(Math.abs(cand-num) < diff) {
                diff = Math.abs(cand-num);
                res = cand;
            } else if(Math.abs(cand-num)==diff) { //it is tie
                res = Math.min(res, cand);
            }
        }
        return res;
    }
}
