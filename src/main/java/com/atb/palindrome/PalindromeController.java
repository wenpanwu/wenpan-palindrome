package com.atb.palindrome;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PalindromeController {


    @Autowired
    private PalindromeService palindromeService;

    @GetMapping(value = "/", produces = { "text/plain" })
    public ResponseEntity<Object> index() {
        return ResponseEntity.ok().body("Hello world");
    }

    /**
     * the closest the palindrome will come from one of the 5 cases.
     * case 0: reverse the left as right
     * case 1: left + 1, reverse the left as right
     * case 2: left - 1, reverse the left as right
     * case 3: pow of 10 len-1 times, minus 1
     * case 4: pow of 10 len times, plus 1
     * Then find the closest one from the 5 candidates, if there is a tie in diff, choose the smaller one
     * The assumptions of the implementation:
     * the number passed in as String is non-negative long, no validation
     * @param number
     * @return
     */
    @GetMapping(value = "/palindrome/{number}")
    public ResponseEntity<Object> palindrome(@PathVariable String number){
        long num = Long.parseLong(number);
        long res = palindromeService.getClosestPalindrome2(num);
        return ResponseEntity.ok().body(String.valueOf(res));
    }


}
