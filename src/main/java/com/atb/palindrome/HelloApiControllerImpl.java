package com.atb.palindrome;

import com.atb.palindrome.model.Hello;
import io.swagger.annotations.ApiParam;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@RestController
public class HelloApiControllerImpl implements HelloApi {
    @Override
    @GetMapping("hello")
    public ResponseEntity<Hello> hello(@NotNull @ApiParam(value = "Person's name", required = true) @Valid @RequestParam(value = "name", required = true) String name) {
        return new ResponseEntity<>(new Hello().text("Hello " + name + "!"), HttpStatus.OK);
    }
}
