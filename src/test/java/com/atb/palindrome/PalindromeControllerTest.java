package com.atb.palindrome;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
@ContextConfiguration(classes = {PalindromeController.class, PalindromeService.class})
@WebMvcTest
public class PalindromeControllerTest {
    @Autowired
    private MockMvc mockMvc;

    //case 0: reverse the left as right
    //case 1: left + 1, reverse the left as right
    //case 2: left - 1, reverse the left as right
    //case 3: pow of 10 len-1 times, minus 1
    //case 4: pow of 10 len times, plus 1
    @ParameterizedTest
    //          case 0      case 2     case 0   case 0             case 2
    @CsvSource({"123,121", "121,111", "12,11", "123456,123321", "666666,665566",
           // case 2          case 3  case 3     case 1    case 2   case 4
            "660066,659956", "10,9", "101,99", "909,919", "19,22", "99,101",
            "4567778970122,4567778777654", "3456050408011,3456050506543"})
    public void testGetClosestPalindrome(String num, String closestNum) throws Exception {
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.get("/palindrome/"+num))
                .andExpect(status().isOk()).andReturn();
        String resultStr = result.getResponse().getContentAsString();
        Assertions.assertEquals(closestNum, resultStr);
    }

}
