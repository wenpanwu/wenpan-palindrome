package com.atb.palindrome;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

//this test in my machine shows second solution is about 1000 times faster than first solution when the number is very big
public class PalindromeServiceTest {
    private PalindromeService palindromeService = new PalindromeService();
    @Test
    public void testGetClosestPalindrome1() {
        long start = System.nanoTime();
        for(int i=0; i<100; i++) {
            long ret1 = palindromeService.getClosestPalindrome1(4567778970122L);
            long ret2 = palindromeService.getClosestPalindrome1(3456050408011L);
            Assertions.assertEquals(4567778777654L, ret1);
            Assertions.assertEquals(3456050506543L, ret2);
        }
        long stop = System.nanoTime();
        System.out.println("duration1="+(stop - start));
    }
    @Test
    public void testGetClosestPalindrome2() {
        long start = System.nanoTime();
        for(int i=0; i<100; i++) {
            long ret1 = palindromeService.getClosestPalindrome2(4567778970122L);
            long ret2 = palindromeService.getClosestPalindrome2(3456050408011L);
            Assertions.assertEquals(4567778777654L, ret1);
            Assertions.assertEquals(3456050506543L, ret2);
        }
        long stop = System.nanoTime();
        System.out.println("duration2="+(stop - start));
    }
}
