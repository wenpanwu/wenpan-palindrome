package com.atb.palindrome;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.ArrayList;

public class Java17Test {
    @Test
    public void testJava17Features() {
        var  myList = new ArrayList<Integer>();
        myList.add(1);
        myList.add(2);
        myList.add(4);
        Assertions.assertEquals(3, myList.size());
        Assertions.assertEquals(4, myList.get(2));
        var localDate = LocalDate.of(2022, 11, 23);
        var dayOfWeek =  localDate.getDayOfWeek();
        var isWorkDay = switch (dayOfWeek) {
            case MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY -> true;
            case SATURDAY, SUNDAY -> false;
        };
        Assertions.assertEquals(true, isWorkDay);
    }
}
